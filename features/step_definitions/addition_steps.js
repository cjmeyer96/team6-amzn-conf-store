const {
  Before,
  Given,
  When,
  Then,
  And,
  After
} = require('cucumber')
const express = require('express')
const app = express()
var assert = require('assert')
var requester = require('../requester')

var user, password;

Given('I have user {string}', function(userGiven) {
  user = userGiven;
});

Given('the password is {string}', function(passwordGiven) {
  password = passwordGiven;
});

Then('the response after the addition attempt should be {string} and the message should be {string}', function(statusGiven, messageGiven) {
  var myUser = user;
  var myPassword = password;
  requester.get('/manage/newuser/' + myUser + '/' + myPassword).then(function(res) {
    assert.equal(res.body.status, statusGiven);
    assert.equal(res.body.messsage, messageGiven);
  }).then(function() {
    requester.get('/manage/rmuser/' + myUser + '/' + myPassword);
  });
});

var actualUser, enteredUser;

Given('I have existing user {string}', function(userGiven) {
  actualUser = userGiven;
});

Given('I try to remove {string}', function(userGiven) {
  enteredUser = userGiven;
});

var actualPass, enteredPass;

Given('the actual password is {string}', function(passGiven) {
  actualPass = passGiven;
});

Given('the password I enter is {string}', function(passGiven) {
  enteredPass = passGiven;
});

Then('the response after the removal attempt should be {string} and the message should be {string}', function(statusGiven, secondValueGiven) {
  var myActualUser = actualUser;
  var myActualPass = actualPass;
  return requester.get('/manage/newuser/' + actualUser + '/' + actualPass).then(function(res) {
    return requester.get('/manage/rmuser/' + enteredUser + '/' + enteredPass).then(function(res) {
      assert.equal(res.body.status, statusGiven);
      assert.equal(Object.values(res.body)[1], secondValueGiven);
    });
  }).then(function() {
    requester.get('/manage/rmuser/' + myActualUser + '/' + myActualPass)
  });
});

var api_key, service, config, value, permissions;

Given('the user has the API key {string}', function(api_keyGiven) {
  api_key = api_keyGiven;
});

Given('the user selects the service {string}', function(serviceGiven) {
  service = serviceGiven
})

Given('the user desires the configuration {string}', function(configGiven) {
  config = configGiven;
})

Given('the new configuration value should be {string}', function(valueGiven) {
  value = valueGiven;
})

Given('the permission level should be {string}', function(permissionsGiven) {
  permissions = permissionsGiven;
})

Then('the status after the new configuration value is set should be {string} and the message should be {string}', function(statusGiven, messageGiven) {
  var myUser = user;
  var myApi_key = api_key;
  var myService = service;
  var myConfig = config;
  var myValue = value;
  var myPermissions = permissions;
  return requester.get('/api/' + myApi_key + '/' + myUser + '/' + myService + '/' + myConfig + '/put/' + myValue + '/' + myPermissions).then(function(res) {
    assert.equal(res.body.status, statusGiven);
    assert.equal(res.body.response, messageGiven);
  }).then(function() {
    requester.get('/api/' + myApi_key + '/' + myUser + '/' + myService + '/' + myConfig + '/rm');
  });
})

var actualService;

Given('an actual service is {string}', function(actualServiceGiven) {
  actualService = actualServiceGiven;
})

Then('the status after getting the configuration value should be {string}', function(statusGiven) {
  var myUser = user;
  var myApi_key = api_key;
  var myService = service;
  var myActualService = actualService;
  var myConfig = config;
  var myValue = 50;
  var myPermissions = 3;
  return requester.get('/api/' + myApi_key + '/' + myUser + '/' + myActualService + '/' + myConfig + '/put/' + myValue + '/' + myPermissions).then(function(res) {
    return requester.get('/api/' + myApi_key + '/' + myUser + '/' + myService + '/' + myConfig + '/get').then(function(res) {
      assert.equal(res.body.status, statusGiven);
    });
  }).then(function() {
    requester.get('/api/' + myApi_key + '/' + myUser + '/' + myActualService + '/' + myConfig + '/rm');
  });
})

Then('the status after the removal attempt of the configuration value should be {string}', function(statusGiven) {
  var myUser = user;
  var myApi_key = api_key;
  var myService = service;
  var myActualService = actualService;
  var myConfig = config;
  var myValue = 50;
  var myPermissions = 3;
  return requester.get('/api/' + myApi_key + '/' + myUser + '/' + myActualService + '/' + myConfig + '/put/' + myValue + '/' + myPermissions).then(function(res) {
    return requester.get('/api/' + myApi_key + '/' + myUser + '/' + myService + '/' + myConfig + '/rm').then(function(res) {
      assert.equal(res.body.status, statusGiven);
    });
  }).then(function() {
    requester.get('/api/' + myApi_key + '/' + myUser + '/' + myActualService + '/' + myConfig + '/rm');
  });
  //IMPORTANT: Removing nonexistent config values is still possible
})

var call;
Given('I make an invalid API call {string}', function(callGiven) {
  call = callGiven;
})

Then('the response should be {string} and the message should be {string}', function(error, message) {
      return requester.get(call).then(function(res) {
        assert.equal(res.body.status, error);
        assert.equal(res.body.error, message);
      })
    });
