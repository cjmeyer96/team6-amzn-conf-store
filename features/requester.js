// requester.js
const fetch = require('node-fetch');
const buildUrl = require('build-url');

// The port number that the Server is listening on
const port = 3000;

// The Base URL to the server, before our request-specific path is added
const baseUrl = 'http://127.0.0.1:' + port;

// The last response that we received
let lastResponse;

// Perform a GET request to the server
function get(url) {
  const realUrl = buildUrl(baseUrl, {
    path: url
  });

  lastResponse = fetch(realUrl)
    .then((res) => {
      // Our response object allows us to decode the body as a JSON
      // object, so let's do that
      return res.json().then((body) => {
        // We only care about the status code, headers and body
        return {
          status: res.status,
          headers: res.headers.raw(),
          body: body
        };
      });
    })
    .catch((err) => {
      return err;
    });

  return lastResponse;
}

// Get the last response that we received
function getLastResponse() {
  return lastResponse;
}

module.exports = {
  get: get,
  getLastResponse: getLastResponse
};
