Feature: Addition

	Addition is great as a verification exercise to get the Cucumber-js infrastructure up and running

	Scenario Outline: New User
		Given I have user <user>
		And the password is <password>
		Then the response after the addition attempt should be <status> and the message should be <message>

		Examples:
      | user				| password		| status		|	message										  	|
      | "johndoe" 	| "hello"			| "success"	| "Added johndoe to database." 	|
			| "janedoe" 	| "hola"			| "success"	| "Added janedoe to database." 	|

	Scenario Outline: Remove User
		Given I have existing user <actualUser>
		And I try to remove <enteredUser>
		And the actual password is <actualPass>
		And the password I enter is <enteredPass>
		Then the response after the removal attempt should be <status> and the message should be <secondValue>

		Examples:
			|	actualUser	|	enteredUser | actualPass	|	enteredPass	|	status		|	secondValue																|
			|	"rafael"		|	"rafael"		|	"hola"			|	"hola"			| "success"	|	"DeleteUser succeeded"										|
			|	"roger"			|	"roger"			|	"hej"				|	"hello"			|	"error"		|	"your username or password was incorrect" |
			|	"novak"			|	"andy"			|	"zdravo" 		|	"zdravo"		| "error"		|	"your username or password was incorrect" |

	Scenario Outline: New Configuration Value
		Given I have user <user>
		And the user has the API key <api_key>
		And the user selects the service <service>
		And the user desires the configuration <config>
		And the new configuration value should be <value>
		And the permission level should be <permissions>
		Then the status after the new configuration value is set should be <status> and the message should be <message>

		Examples:
		|	user			|	api_key		|	service			|	config	|	value		|	permissions	|	status		|	message				|
		|	"simon"		|	"abcdef"	|	"garfunkel"	|	"hall"	|	"oates"	|	"3"					|	"success"	|	"Added item"	|
		|	"john"		|	"qwerty"	|	"paul"			|	"ringo"	|	"67"		|	"1"					|	"success"	|	"Added item"	|

	Scenario Outline: Get Configuration Value
		Given I have user <user>
		And the user has the API key <api_key>
		And the user selects the service <service>
		And an actual service is <actualService>
		And the user desires the configuration <config>
		Then the status after getting the configuration value should be <status>

		Examples:
		|	user			|	api_key		|	service			|	actualService	|	config	|	status		|
		|	"simon"		|	"abcdef"	|	"garfunkel"	|	"garfunkel"		|	"hall"	|	"success"	|
		|	"john"		|	"qwerty"	|	"paul"			|	"george"			|	"ringo"	|	"error"		|

	Scenario Outline: Remove Configuration Value
		Given I have user <user>
		And the user has the API key <api_key>
		And the user selects the service <service>
		And an actual service is <actualService>
		And the user desires the configuration <config>
		Then the status after the removal attempt of the configuration value should be <status>

		Examples:
		|	user			|	api_key		|	service			|	actualService	|	config	|	status		|
		|	"simon"		|	"abcdef"	|	"garfunkel"	|	"garfunkel"		|	"hall"	|	"success"	|

	Scenario Outline: Random Invalid API Call
		Given I make an invalid API call <call>
		Then the response should be "error" and the message should be "unknown api call"

		Examples:
		|	call															|
		|	"/invalid/api/call"								|
		|	"/manage/newrando/elon/musk"			|
		|	"/manage/rm/bill/gates"						|
		|	"/api/abcdef/kim/khloe/kris/add/"	|
