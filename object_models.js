/*
    CONFIGURATION VALUE OBJECT
    getCurrent: Returns latest set value
    getDatabaseFormat: Returns JSON format of object which can be saved to database
*/

function get_config_value_object_from_json(json) {
  var original_object = JSON.parse(json);
  var new_object = new config_value(original_object.owner, original_object.current, original_object.type)
  new_object.permissions = original_object.permissions
  new_object.past = new_object.past.concat(original_object.past)
  return new_object
}

var passwordHash = require('password-hash');
class config_value {
  constructor(owner, value, type) {
    this.owner = owner;
    this.current = value;
    this.type = type;
    this.permissions = {}
    this.past = [];
  }

  update_value(value) {
    if (this.current != null) {
      this.past.push(this.current)
    }
    this.current = value;
  }

  getDatabaseFormat() {
    return JSON.stringify(this);
  }

  getCurrent() {
    return this.current;
  }

  addPermission(username, user_to_grant, level) {
    if (this.owner == username) {
      this.permissions[user_to_grant] = new permission(user_to_grant, level)
      return true;
    } else {
      return false;
    }
  }

  canWrite(username) {
    try {
      if (this.owner == username) {
        return true;
      }
      return this.permissions[username].level >= 2
    } catch (err) {
      return false;
    }
  }

  getCurrent() {
      return this.current;
  }

  addPermission(username, user_to_grant, level) {
      if (this.owner == username) {
          /*if (this.permissions[username] == null) {
              this.permissions[username] = new permission(username, 2)
          }*/

          this.permissions[user_to_grant] = new permission(user_to_grant, level)
          return true;
      } else {
          return false;
      }
  }

  canWrite(username) {
      try {
          if (this.owner == username) {

              // Upon first entry, assign correct permissions to owner.
              if (this.permissions[username] == undefined) {
                  this.permissions[username] = new permission(username, 2)
              }

              return true;
          }

          return this.permissions[username].level >= 2
      } catch(err){
          return false;
      }
  }

  canRead(username) {
      try {
          if (this.owner == username) {
              return true;
          }
          return this.permissions[username].level >= 1
      } catch(err){
          return false;
      }
    }
  }


/*
    CONFIGURATION VALUE OBJECT
    getDatabaseFormat: Returns JSON format of object which can be saved to database
*/
class config_identifier {
  constructor(user, service, config_name) {
    this.user = user;
    this.service = service;
    this.config_name = config_name;
  }

  getDatabaseFormat() {
    return JSON.stringify(this);
  }
}

class api_key {
  constructor(username) {
    this.username = username;
    this.key = passwordHash.generate(username)
    this.timeout = new Date((new Date).getTime() + 10 * 60000);
  }

  getKey() {
    return this.key;
  }

  getDatabaseFormat() {
    return JSON.stringify(this);
  }
}

class permission {
  constructor(username, level) {
    this.username = username;
    this.level = level;
  }
}

module.exports = {
  config_value: config_value,
  config_identifier: config_identifier,
  api_key: api_key,
  get_config_value_object_from_json: get_config_value_object_from_json
}
