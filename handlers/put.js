// put.js
// ========
var database = require("./database")
var models = require("../object_models")
var auth = require("./auth")

module.exports = {
    addPerm: function(res, req) {
        var username = req.params.username
        var api_key = req.params.api_key;
        var user = req.params.user;
        var service_name = req.params.service_name;
        var config_name = req.params.config_name;
        var user_to_grant = req.params.user_to_grant;
        var level = req.params.level;

        var config_identifier  = new models.config_identifier(user, service_name, config_name);
        database.get_config_value(config_identifier.getDatabaseFormat()).then(function(data){

            if (Object.keys(data).length != 0) {
                var current_config_value = JSON.parse(data.Item.value.S);
                var new_config_value = new models.config_value(username, current_config_value.current, current_config_value.type);
                new_config_value.permissions = current_config_value.permissions
                if (new_config_value.permissions == undefined) {
                    new_config_value.permissions = {}
                }
                new_config_value.past = new_config_value.past.concat(current_config_value.past)
            }

            if (new_config_value != undefined) {
              auth.authorize_key(username, api_key).then(function(authorized) {
                  if (authorized) {
                      if (new_config_value.addPermission(username, user_to_grant, level)) {
                          database.put_config_value(config_identifier.getDatabaseFormat(), new_config_value.getDatabaseFormat()).then(function(data){
                              res.send({status: "success", response: data})
                          }).catch(function(err){
                              res.send({status: "error", error: "there was a backend error - " + err})
                          })
                      } else {
                          res.send({status: "error", error: "only the owner may add permissions"})
                      }
                  } else {
                      res.send({status: "error", error: "your api key is either invalid for this username or has expired"})
                  }
              })
            } else {
              res.send({status: "error", error: "you cannot set permissions for a configuration that does not exist"})
            }
        })
    },

    doPut: function (res, req) {
        var username = req.params.username
        var api_key = req.params.api_key;
        var user = req.params.user;
        var service_name = req.params.service_name;
        var config_name = req.params.config_name;
        var value = req.params.value;
        var type = req.params.type;

        var config_identifier  = new models.config_identifier(user, service_name, config_name);

        database.get_config_value(config_identifier.getDatabaseFormat()).then(function(data){
            var new_config_value = new models.config_value(username, value, type);

            if (Object.keys(data).length != 0) {
                var current_config_value = JSON.parse(data.Item.value.S);
                new_config_value.permissions = current_config_value.permissions
                new_config_value.past.push({date: new Date().toString(), value: current_config_value.current})
                new_config_value.past = new_config_value.past.concat(current_config_value.past)
            }

            if (current_config_value != undefined && (current_config_value.permissions[username] == undefined || current_config_value.permissions[username].level < 2)) {
                res.send({status: "error", error: "your user does not have the correct permissions for this action"})
            } else {
                auth.authorize_key(username, api_key).then(function(authorized) {
                    if (authorized) {
                        if (new_config_value.canWrite(username)) {
                            database.put_config_value(config_identifier.getDatabaseFormat(), new_config_value.getDatabaseFormat()).then(function(data){
                                res.send({status: "success", response: data})
                            }).catch(function(err){
                                res.send({status: "error", error: "there was a backend error - " + err})
                            })
                        } else {
                            res.send({status: "error", error: "your user does not have the correct permissions for this action"})
                        }

                    } else {
                        res.send({status: "error", error:"your api key is either invalid for this username or has expired"})
                    }
                })
            }
        })
    }
};
