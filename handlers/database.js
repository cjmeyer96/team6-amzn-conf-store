// auth.js
// ========
var AWS = require("aws-sdk");
var AmazonDaxClient = require('amazon-dax-client')
var region = "us-east-2";

AWS.config.update({
    region: region,
    accessKeyId: "AKIAJYJAT4MBKDWWGN5Q",
    secretAccessKey: "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"
});

// Check
const shell = require('shelljs');
var isEC2 = /us-east-2.compute.internal/.test(shell.exec('hostname -d').stdout)
// var isEC2 = false; //Disable DAX for money sake ($$)

client = new AWS.DynamoDB();
var client = null;
if (isEC2) {
    console.log("Running on an EC2 Instance!")

    // DAX client init.
    var client = new AmazonDaxClient({endpoints: ["amz-dax-clust-lrg.4em3ow.clustercfg.dax.use2.cache.amazonaws.com:8111"], region: region})
    // client = new AWS.DynamoDB({service: daxEndpoint});
} else {
    console.log("Not Running on an EC2 Instance!")
    client = new AWS.DynamoDB();
}

module.exports = {

    get_config_value: function (identifier) {
        return new Promise(function (resolve, reject) {

            var params = {
                TableName: "configurations",
                Key: {
                    "configuration": {
                      S: identifier
                     }
                }
            };

            client.getItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to query. Error:", JSON.stringify(err));
                } else {
                    resolve(data)
                }
            });
        })
    },

    get_api_key: function (key) {
        return new Promise(function (resolve, reject) {

            var params = {
                TableName: "api_keys",
                Key: {
                     "key": {
                         S: key
                     }
                }
            };

            client.getItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to query. Error:", JSON.stringify(err));
                } else {
                    resolve(data)
                }
            });
        })
    },

    put_api_key: function (key, keyinfo) {
        return new Promise(function (resolve, reject) {
            var table = "api_keys";

            var params = {
                TableName: table,
                Item: {
                    "key": {
                        S: key
                    },
                    "keyinfo": {
                        S: keyinfo
                    }
                }
            };

            console.log("Adding a new item...");
            client.putItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to add item. Error JSON:", JSON.stringify(err));
                } else {
                    resolve("Added item");
                }
            });
        })
    },

    put_config_value: function (identifier, value) {
        return new Promise(function (resolve, reject) {
            var table = "configurations";

            var params = {
                TableName: table,
                Item: {
                    "configuration": {
                        S: identifier
                    },
                    "value": {
                        S: value
                    }
                }
            };

            console.log("Adding a new item...");
            client.putItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to add item. Error JSON:", JSON.stringify(err));
                } else {
                    resolve("Added item");
                }
            });
        })
    },

    remove_config_value: function (identifier) {
        return new Promise(function (resolve, reject) {
            var table = "configurations";

            var params = {
                TableName: table,
                Key: {
                    "configuration": {
                        S: identifier
                    }
                }
            };

            console.log("Attempting a conditional delete...");
            client.deleteItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to delete item. Error JSON:", JSON.stringify(err));
                } else {
                    resolve("DeleteItem succeeded");
                }
            });
        })
    },

    make_user: function (user, password) {
        return new Promise(function (resolve, reject) {
            var table = "users";

            var params = {
                TableName: table,
                Item: {
                    "username": {
                        S: user
                    },
                    "password": {
                        S: password
                    }
                }
            };

            console.log("Adding a new item...");
            client.putItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to add item. Error JSON:", JSON.stringify(err));
                } else {
                    resolve("Added " + user + " to database.");
                }
            });
        })
    },

    get_user: function (username) {
        return new Promise(function (resolve, reject) {
            var params = {
                TableName: "users",
                Key: {
                    "username": {
                      S: username
                     }
                   },
            };

            client.getItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to query. Error:", JSON.stringify(err));
                } else {
                    // console.log("Query succeeded.");
                    // data.Items.forEach(function (item) {
                    //     console.log(item);
                    // });
                    resolve(data)
                }
            });
        })
    },

    remove_user: function (username) {
        return new Promise(function (resolve, reject) {
            var table = "users";

            var params = {
                TableName: table,
                Key: {
                    "username": {
                        S: username
                    }
                }
            };

            console.log("Attempting a conditional delete...");
            client.deleteItem(params, function (err, data) {
                if (err) {
                    console.log(err)
                    reject("Unable to delete item. Error JSON:", JSON.stringify(err));
                } else {
                    resolve("DeleteUser succeeded");
                }
            });
        })
    }
};
