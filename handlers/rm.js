// rm.js
// ========
var database = require("./database")
var models = require("../object_models")
var auth = require("./auth")

module.exports = {
    doRm: function (res, req) {
        var username = req.params.username
        var api_key = req.params.api_key;
        var user = req.params.user;
        var service_name = req.params.service_name;
        var config_name = req.params.config_name;

        auth.authorize_key(username, api_key).then(function(authorized) {
            if (authorized) {

            var config_identifier  = new models.config_identifier(user, service_name, config_name)
            database.remove_config_value(config_identifier.getDatabaseFormat()).then(function(data){
                if (data != null) {
                    res.send({status: "success", response: data})
                } else {
                    res.send({status: "error", error: "configuration not found"})
                }
            }).catch(function(err){
                res.send({status: "error", error: "there was a backend error - " + err})
            })
        } else {
            res.send({status: "error", error:"your api key is either invalid for this username or has expired"})
        }
    })
    }
};
