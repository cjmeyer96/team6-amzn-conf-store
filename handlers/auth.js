// auth.js
// ========
var database = require("./database")
var passwordHash = require('password-hash');
var models = require("../object_models")

module.exports = {
  authorize_user: function (username, password) {
    return new Promise(function(resolve, reject) {
      database.get_user(username).then(function(data) {
        if (Object.keys(data).length == 0) {
          resolve(false)
        } else {
          resolve(passwordHash.verify(password, data.Item.password.S))
        }
      }).catch(function(err){
        console.log(err)
      })


    });
  },

  authorize_key: function (username, key) {
    return new Promise(function(resolve, reject) {
        database.get_api_key(key).then(function(data){
          if (Object.keys(data).length == 0) {
            resolve(false)
          } else {
          if (new Date(JSON.parse(data.Item.keyinfo.S).timeout) >= new Date()) {
            resolve(passwordHash.verify(username, key))
          } else {
            resolve(false)
          }
        }
      })
    })
  },

  generatekey: function (res, req) {
    var username = req.params.username;
    var password = req.params.password;

    this.authorize_user(username, password).then(function(correctcredentials) {
      if (correctcredentials) {
        var key = new models.api_key(username);
        database.put_api_key(key.getKey(), key.getDatabaseFormat()).then(function(data){
          res.send({ status: "success", key: key.getKey()})
        }).catch(function(err) {
          res.send({status: "error", error: "there was a backend error - " + err})
        })
      } else {
        res.send({status: "error", error: "your username or password was invalid"})
      }
    })
  },

  login: function(res, req) {
    var username = req.params.username;
    var password = req.params.password;

    database.get_user(username).then(function(data){
      if (Object.keys(data).length == 0) {
        res.send({status: "error", error: "user not found"})
      } else {
        if (passwordHash.verify(password, data.Item.password.S)) {
          res.send({status: "success", response: "valid"})
        } else {
          res.send({status: "error", response: "your username/password combination does not match"})
        }
      }
    })
  },

  newuser: function (res, req) {
    var username = req.params.username;
    var password = req.params.password;

    database.make_user(username, passwordHash.generate(password)).then(function(data){
      res.send({ status: "success", messsage: data})
    }).catch(function(err) {
      res.send({status: "error", error: "there was a backend error - " + err})
    })
  },

  rmuser: function (res, req) {
    var username = req.params.username;
    var password = req.params.password;

    this.authorize_user(username, password).then(function(valid) {
      if (valid) {
        database.remove_user(username).then(function(data){
          res.send({ status: "success", messsage: data})
        }).catch(function(err) {
          res.send({status: "error", error: "there was a backend error - " + err})
        })
      } else {
        res.send({status: "error", error: "your username or password was incorrect"})
      }
    })
  },


};
