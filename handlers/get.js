// get.js
// ========
var database = require("./database")
var models = require("../object_models")
var auth = require("./auth")

module.exports = {
    doGet: function (res, req) {
        var username = req.params.username
        var api_key = req.params.api_key;
        var user = req.params.user;
        var service_name = req.params.service_name;
        var config_name = req.params.config_name;

        auth.authorize_key(username, api_key).then(function(authorized) {
            if (authorized) {
            var config_identifier  = new models.config_identifier(user, service_name, config_name)
            database.get_config_value(config_identifier.getDatabaseFormat()).then(function(data){
                if (models.get_config_value_object_from_json(data.Item.value.S).canRead(username)) {
                    if (Object.keys(data).length == 0) {
                        res.send({status: "error", error: "configuration not found"})
                    } else {
                        res.send({status: "success", response: data})
                    }
                } else {
                    res.send({status: "error", error: "your user does not have the required permissions to read this value"})
                }

            }).catch(function(err){
                res.send({status: "error", error: "there was a backend error - " + err})
            })
        } else {
            res.send({status: "error", error:"your api key is either invalid for this username or has expired"})
        }
    })
    }
};
