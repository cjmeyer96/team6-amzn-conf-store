const express = require('express');
const app = express();

var get = require('./handlers/get');
var put = require('./handlers/put');
var rm = require('./handlers/rm');
var auth = require("./handlers/auth")

/*
    API: New User - API Key Action
    /manage/newuser/:username/:password

*/
app.get('/manage/newuser/:username/:password', function(req, res) {
  auth.newuser(res, req)
});

/*
    API: Remove User - API Key Action
    /manage/rmuser/:username/:password

*/
app.get('/manage/rmuser/:username/:password', function(req, res) {
  auth.rmuser(res, req)
});

/*
    API: Login User - API Key Action
    /manage/login/:username/:password

*/
app.get('/manage/login/:username/:password', function(req, res) {
  auth.login(res, req)
});

/*
    API: Generate API Key
    /manage/genkey/:username/:password

*/
app.get('/manage/genkey/:username/:password', function(req, res) {
  auth.generatekey(res, req)
});

/*
    API: Add Permission
    /manage/:username/:api_key/addPerm/:user/:service_name/:config_name/:user_to_grant/:level

*/
app.get('/manage/:username/:api_key/addperm/:user/:service_name/:config_name/:user_to_grant/:level', function(req, res) {
  put.addPerm(res, req);
})

/*
    API: PUT Action
    /api/:username/:api_key/:user/:service_name/:config_name/put/:value/:type

*/
app.get('/api/:username/:api_key/:user/:service_name/:config_name/put/:value/:type', function(req, res) {
  put.doPut(res, req);
});

/*
	API: GET Action
	/api/:username/:api_key/:user/:service_name/:config_name/get

*/
app.get('/api/:username/:api_key/:user/:service_name/:config_name/get', function(req, res) {
  get.doGet(res, req);
});

/*
	API: REMOVE Action
	/api/:username/:api_key/:user/:service_name/:config_name/rm

*/
app.get('/api/:username/:api_key/:user/:service_name/:config_name/rm', function(req, res) {
  rm.doRm(res, req);
});

/*
    The 404 Route
    Always Keep this as the last rule

*/
app.get('*', function(req, res) {
  res.send({
    status: "error",
    error: "unknown api call"
  });
});

/*
    Server Init
    Port 3000
*/
app.listen(process.env.PORT || 3000, function() {
  console.log('API Up on port: ' + this.address().port);
});
